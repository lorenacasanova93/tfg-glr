package JFlexCup;
import com.github.jhoenicke.javacup.runtime.ComplexSymbolFactory;
import com.github.jhoenicke.javacup.runtime.ComplexSymbolFactory.Location;
import com.github.jhoenicke.javacup.runtime.Symbol;
import java.io.InputStream;
import java.io.InputStreamReader;

%%

 

%class Lexer
%unicode
%cup

 

EOL = \r | \n | \r\n
SPACE = [\t ]
ARTICULO = "el" | "la" | "los" | "las" | "un" | "una" | "unos" | "unas"
SUSTANTIVO = "piramide" | "caballo" | "trabajadores" | "sacos" | "cubo" | "cuidado" | "gorra" | "arriba" | "policias" | "criminales" | "rifles" | "albaniles"
ADJETIVO = "gran"
VERBO = "galopa" | "soltaron" | "detuvieron"
ADVERBIO = "rapidamente"
PREPOSICION = "a" | "ante" | "bajo" | "cabe" | "con" | "contra" | "de" | "desde" | "en" | "entre" | "hacia" | "hasta" | "mediante" | "para" | "por" | "segun" | "sin" | "sobre" | "tras" | "via"

 

%%
<YYINITIAL> {
	<<EOF>>     {return new Symbol(sym.EOF);}
    {EOL}        {return new Symbol(sym.EOL);}
    {SPACE}        {}
    {ARTICULO}   {return new Symbol(sym.ARTICULO);}
    {SUSTANTIVO}   {return new Symbol(sym.SUSTANTIVO);}
    {ADJETIVO}   {return new Symbol(sym.ADJETIVO);}
    {VERBO}   {return new Symbol(sym.VERBO);}
    {ADVERBIO}   {return new Symbol(sym.ADVERBIO);}
    {PREPOSICION}   {return new Symbol(sym.PREPOSICION);}
}