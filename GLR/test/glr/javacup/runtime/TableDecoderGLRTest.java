package glr.javacup.runtime;
import glr.javacup.runtime.TableDecoderGLR;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class TableDecoderGLRTest extends TestCase {
    String tables = "0,2,1,4|0,22,23,12,49/1,34,22_9,9,11/8_23,26,1_2,55,66|-1,-1,-1/-1,5,-1/4,5,3/7,-1,7";
    TableDecoderGLR decoder = new TableDecoderGLR(tables);
    
    @Test
    public void testDecodeProductionTable(){
        int[] result = {0,2,1,4};
        Assert.assertArrayEquals(decoder.DecodeProductionTable(), result);
    }
    
    @Test
    public void testDecodeActionTable(){
        int[][][] result = {{{0},{22},{23},{12},{49}},{{1},{34},{22,9},{9},{11}},{{8,23},{26},{1,2},{55},{66}}};
        Assert.assertArrayEquals(decoder.DecodeActionTable(), result);
    }
    
    @Test
    public void testDecodeReductionTable(){
        int[][] result = {{-1,-1,-1},{-1,5,-1},{4,5,3},{7,-1,7}};
        Assert.assertArrayEquals(decoder.DecodeReductionTable(), result);
    }
}