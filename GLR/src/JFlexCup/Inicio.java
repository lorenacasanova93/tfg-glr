package JFlexCup;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.StringReader;

import glr.javacup.runtime.ScannerBuffer;
import java.util.ArrayList;

public class Inicio {
    public static void main (String [] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(args[0]));
            String sentence = reader.readLine();
            int i = 1;
            long start_time = System.currentTimeMillis();
            while(sentence != null){
                System.out.println("---Frase "+i+": \""+sentence+"\""+"---");
                ScannerBuffer buffer = new ScannerBuffer(new Lexer(new StringReader(sentence)));
                parser p = new JFlexCup.parser(buffer);
                Object results = p.parse();
                if(results instanceof ArrayList) System.out.println(prettyPrintResults((ArrayList<ArrayList<Integer>>)results));
                System.out.println("Terminales leidos:" + buffer.getBuffered());
                System.out.println("----------------------\n");
                sentence = reader.readLine();
                i++;
            }
            System.out.println("Execution time: " + (System.currentTimeMillis() - start_time) + " miliseconds.");
        }catch (Exception e) {
                System.err.println("Excepcion: "+ e.getMessage());
        }
    }
    
    /** Provides a string representation of the parse results    
    *
    * @param results the list of results that has been composed during the parsing
    */
    private static String prettyPrintResults(ArrayList<ArrayList<Integer>> results)
    {
        String prettyprint ="";
        
        if(results.isEmpty())
            return "All branches resulted in a syntax error";
        
        for(ArrayList<Integer> result : results)
        {
            prettyprint+="[";
            for(int nonterminal : result)
            {
                switch(nonterminal){
                    case 0:
                        prettyprint+="$, ";
                        break;
                    case 1:
                        prettyprint+="Frase, ";
                        break;
                    case 2:
                        prettyprint+="Np, ";
                        break;
                    case 3:
                        prettyprint+="Vp, ";
                        break;
                    case 4:
                        prettyprint+="Pp, ";
                        break;
                    default:
                        break;
                }
                
            }
            prettyprint = prettyprint.substring(0,prettyprint.length()-2);
            prettyprint+="],\n";
        }
        prettyprint = prettyprint.substring(0,prettyprint.length()-2) + "\nNumero de derivaciones: " + results.size();
        
        return prettyprint;
    }
}
