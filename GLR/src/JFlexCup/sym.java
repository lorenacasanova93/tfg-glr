
//----------------------------------------------------
// The following code was generated by jh-javacup-1.2 20210807
// Thu May 04 18:25:15 CEST 2023
//----------------------------------------------------

package JFlexCup;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int error = 0;
  public static final int EOF = 1;
  public static final int SUSTANTIVO = 2;
  public static final int VERBO = 3;
  public static final int ARTICULO = 4;
  public static final int ADJETIVO = 5;
  public static final int ADVERBIO = 6;
  public static final int PREPOSICION = 7;
}

