package JFlexCup;
import glr.javacup.runtime.ComplexSymbolFactory;
import glr.javacup.runtime.ComplexSymbolFactory.Location;
import glr.javacup.runtime.Symbol;
import java.io.InputStream;
import java.io.InputStreamReader;

%%

 

%class Lexer
%unicode
%cup

 

EOL = \r | \n | \r\n
SPACE = [\t ]
ARTICULO = "el" | "la" | "los" | "las" | "un" | "una" | "unos" | "unas" | "este" | "esta" | "estos" | "estas" | "ese" | "esa" | "esos" | "esas" | "aquel" | "aquella" | "aquellos" | "aquellas"
SUSTANTIVO = "piramide" | "caballo" | "trabajadores" | "sacos" | "cubo" | "cuidado" | "gorra" | "arriba" | "policias" | "criminales" | "rifles" | "albaniles" | "elefante" | "pijama" | "hombre" | "parque" | "telescopio" | "cartas" | "casa" | "noche" | "mesa" | "libros" | "cintas" | "centro" | "frase" | "significados" | "sentido" | "humanos"
ADJETIVO = "gran" | "muchos"
VERBO = "galopa" | "soltaron" | "detuvieron" | "quiero" | "dispare" | "vi" | "jugamos" | "sostiene" | "tiene"
ADVERBIO = "rapidamente" | "si" | "no"
PREPOSICION = "a" | "ante" | "bajo" | "cabe" | "con" | "contra" | "de" | "desde" | "en" | "entre" | "hacia" | "hasta" | "mediante" | "para" | "por" | "segun" | "sin" | "sobre" | "tras" | "via"

 

%%
<YYINITIAL> {
	<<EOF>>     {return new Symbol(sym.EOF);}
    {SPACE}        {}
    {ARTICULO}   {return new Symbol(sym.ARTICULO);}
    {SUSTANTIVO}   {return new Symbol(sym.SUSTANTIVO);}
    {ADJETIVO}   {return new Symbol(sym.ADJETIVO);}
    {VERBO}   {return new Symbol(sym.VERBO);}
    {ADVERBIO}   {return new Symbol(sym.ADVERBIO);}
    {PREPOSICION}   {return new Symbol(sym.PREPOSICION);}
}