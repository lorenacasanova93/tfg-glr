package glr.javacup;

/** This class represents one part (either a symbol or an action) of a 
 *  production.  This abstract base class it actually empty.
 *
 * @see     glr.javacup.Grammar
 * @version last updated: 11/25/95
 * @author  Scott Hudson
 */
public abstract class production_part {

  /*-----------------------------------------------------------*/
  /*--- Constructor(s) ----------------------------------------*/
  /*-----------------------------------------------------------*/
       
  /** Simple constructor. */
  public production_part()
    {
    }
}
