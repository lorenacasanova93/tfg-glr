
package glr.javacup;

/** This class represents the complete "action" table of the parser. 
 *  It has one row for each state in the parse machine, and a column for
 *  each terminal symbol.  Each entry in the table represents a shift,
 *  reduce, or an error.  
 *  
 *  An error action is represented by 0 (ERROR), a shift action by
 *  odd numbers, i.e. 2*to_state.index() + SHIFT, and a reduce action
 *  by positive even numbers, i.e. 2*prod.index() + REDUCE.
 *  You can use the static function action, isReduce, isShift and index
 *  to manipulate action codes. 
 *
 * @author  Scott Hudson, Jochen Hoenicke, Lorena Casanova
 */
public class parse_action_table_GLR {

  /** Actual array of rows, one per state. */
  public final int[][][] table;
  
  /** Constant for action type -- error action. */
  public static final int ERROR = 0;

  /** Constant for action type -- shift action. */
  public static final int SHIFT = 1;

  /** Constants for action type -- reduce action. */
  public static final int REDUCE = 2;
  
  /*-----------------------------------------------------------*/
  /*--- Constructor(s) ----------------------------------------*/
  /*-----------------------------------------------------------*/

  /** Simple constructor.  All terminals, non-terminals, and productions must 
   *  already have been entered, and the viable prefix recognizer should
   *  have been constructed before this is called.
   */
  public parse_action_table_GLR(Grammar grammar)
    {
      /* determine how many states we are working with */
      int _num_states = grammar.lalr_states().size();
      int _num_terminals = grammar.num_terminals();

      /* allocate the array and fill it in with empty rows */
      table = new int[_num_states][_num_terminals+1][1];
    }

  /*-----------------------------------------------------------*/
  /*--- General Methods ---------------------------------------*/
  /*-----------------------------------------------------------*/
  /**
   * Returns the action code for the given kind and index.
   * @param kind the kind of the action: ERROR, SHIFT or REDUCE.
   * @param index the index of the destination state resp. production rule.
   */
  public static int action(int kind, int index)
    {
      return 2*index + kind;
    }
  /**
   * Returns true if the code represent a reduce action
   * @param code the action code.
   * @return true for reduce actions.
   */
  public static boolean isReduce(int code)
    {
      return code != 0 && (code & 1) == 0;
    }
  /**
   * Returns true if the code represent a shift action
   * @param code the action code.
   * @return true for shift actions.
   */
  public static boolean isShift(int code)
    {
      return (code & 1) != 0;
    }
  /**
   * Returns the index of the destination state of SHIFT resp. 
   * reduction rule of REDUCE actions.
   * @param code the action code.
   * @return the index.
   */
  public static int index(int code)
    {
      return ((code-1) >> 1);
    }
  
  public static String toString(int code)
    {
      if (code == ERROR)
	return "ERROR";
      else if (isShift(code))
	return "SHIFT("+index(code)+")";
      else
	return "REDUCE("+index(code)+")";
    }

  /*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

  /*-----------------------------------------------------------*/

}
