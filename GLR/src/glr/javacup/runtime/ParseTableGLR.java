package glr.javacup.runtime;

/**
 * Class to hold the action, reduce, and production tables needed by the GLR parser.
 * 
 * @author Lorena Casanova
 */
public final class ParseTableGLR {
  private int[][][] _action_table; 
  private int[][] _reduce_table; 
  private int[] _production_table; 

  public ParseTableGLR(String tables) 
    {
      TableDecoderGLR decoder = new TableDecoderGLR(tables);
      _production_table = decoder.DecodeProductionTable();
      _action_table = decoder.DecodeActionTable();
      _reduce_table = decoder.DecodeReductionTable();
    }

  /** Fetch an action from the action table.    
   *
   * @param state the state index of the action being accessed.
   * @param sym   the Symbol index of the action being accessed.
   */
  final int[] getAction(int state, int sym)
    {
      return _action_table[state][sym];
    }

  /*. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .*/

  /** Fetch a state from the reduce-goto table.    
   *
   * @param state the state index of the entry being accessed.
   * @param sym   the Symbol index of the entry being accessed.
   */
  final int getReduce(int state, int sym)
    {
      return _reduce_table[state][sym];
    }

  /** Get the symbol that a production produces.
   *
   * @param rule the rule number (value in action table).
   */
  final int getProductionSymbol(int rule)
    {
      return _production_table[2 * rule];
    }

  /** Get the number of tokens a production removes from the stack.
   *
   * @param rule the rule number (value in action table).
   */
  final int getProductionSize(int rule)
    {
      return _production_table[2 * rule + 1];
    }
}
