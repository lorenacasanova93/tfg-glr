package glr.javacup.runtime;
import java.util.Arrays;
import java.util.regex.*;

/**
 * Auxiliary class to decode table strings through Regex
 * 
 * @author Lorena Casanova
 */
final class TableDecoderGLR {
  final private String _productionTable;
  final private String _actionTable;
  final private String _reduceTable;
  final private Pattern _splitTables = Pattern.compile("[^|]+");
  final private Pattern _splitRows = Pattern.compile("[^\\/]+");
  final private Pattern _splitCells = Pattern.compile("[^,]+");
  final private Pattern _splitGLR = Pattern.compile("[^_]+");
  private Matcher _matcher;
  private Matcher _rowMatcher;
  private Matcher _glrMatcher;
  private int _rowSize;
  private int _colSize;
  
  public TableDecoderGLR(String tables) 
  {
    _matcher = _splitTables.matcher(tables);
    _matcher.find();
    _productionTable = _matcher.group();
    _matcher.find();
    _actionTable = _matcher.group();
    _matcher.find();
    _reduceTable = _matcher.group();
  }
  
  public int[] DecodeProductionTable(){
      _matcher = _splitCells.matcher(_productionTable);
      _colSize = Math.toIntExact(_matcher.results().count());
      int[] table = new int[_colSize];
      int i = 0;
      
      _matcher.reset();
      while(_matcher.find()){
          table[i] = Integer.parseInt(_matcher.group());
          i++;
      }
      
      return table;
  }
  
  public int[][][] DecodeActionTable(){
      _rowMatcher = _splitRows.matcher(_actionTable);
      _colSize = Math.toIntExact(_rowMatcher.results().count());
      String [] rows = new String[_colSize];
      int i = 0, j = 0, k = 0;
      
      _rowMatcher.reset();
      while(_rowMatcher.find()){
          rows[i] = _rowMatcher.group();
          i++;
      }
      
      _matcher = _splitCells.matcher(rows[0]);
      _rowSize = Math.toIntExact(_matcher.results().count());
      
      int[][][] table = new int[_colSize][_rowSize][1];
      
      _matcher.reset();
      _rowMatcher.reset();
      
      for(i = 0; i < rows.length ; i++){
          _matcher = _splitCells.matcher(rows[i]);
          while(_matcher.find()){
              _glrMatcher = _splitGLR.matcher(_matcher.group());
              while(_glrMatcher.find()){
                if(k > 0)
                    table[i][j] = Arrays.copyOf(table[i][j], table[i][j].length+1);
                table[i][j][k] = Integer.parseInt(_glrMatcher.group());
                k++;
              }
              k=0;
              j++;
          }
          j=0;
      }
      
      return table;
  }
  
  public int[][] DecodeReductionTable(){
      _rowMatcher = _splitRows.matcher(_reduceTable);
      _colSize = Math.toIntExact(_rowMatcher.results().count());
      String [] rows = new String[_colSize];
      int i = 0, j = 0;
      
      _rowMatcher.reset();
      while(_rowMatcher.find()){
          rows[i] = _rowMatcher.group();
          i++;
      }
      
      _matcher = _splitCells.matcher(rows[0]);
      _rowSize = Math.toIntExact(_matcher.results().count());
      
      int[][] table = new int[_colSize][_rowSize];
      
      _matcher.reset();
      _rowMatcher.reset();
      
      for(i = 0; i < rows.length ; i++){
          _matcher = _splitCells.matcher(rows[i]);
          while(_matcher.find()){
              table[i][j] = Integer.parseInt(_matcher.group());
              j++;
          }
          j=0;
      }
      
      return table;
  }
}
