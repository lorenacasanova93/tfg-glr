package glr.javacup.runtime;

import java.util.ArrayList;

/**
 * This class encapsulates the information each branch needs to keep track of independently
 * parse_state: At what parse state is this branch at
 * stack: The stack of symbols this branch has
 * action: The action it will next take
 * result: The list of ids of nonterminals resulting of the parsing
 * @author Lorena Casanova
 */
public class GLRBranch {
    public int parse_state;
    public ArrayList<Symbol> stack;
    public int action;
    public ArrayList<Integer> result;
    
    public GLRBranch(int state, ArrayList<Symbol> stack, ArrayList<Integer> results){
        parse_state = state;
        this.stack = stack;
        action = 0;
        result = results;
    }
    
    public GLRBranch copy(){
        return new GLRBranch(this.parse_state, new ArrayList<Symbol>(this.stack), new ArrayList<Integer>(this.result));
    }
    
    public void pushStack(Symbol symbol){
        stack.add(symbol);
    }
    
    public Symbol popStack(int index){
        return stack.remove(index);
    }
    
    public void pushResult(int id){
        result.add(id);
    }
}
